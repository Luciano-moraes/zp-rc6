***Settings***
Documentation         Representação de variaveis page object

***Variables***
${NAV_CUSTOMERS}      css:a[href$=customers] 
${NAV_EQUIPOS}        css:a[href$=equipos] 


***Keywords***

Go To Customers
    Wait Until Element Is Visible      ${NAV_CUSTOMERS}          5
    Click Element                      ${NAV_CUSTOMERS}

Go To Equipos
    Wait Until Element Is Visible      ${NAV_EQUIPOS}         5
    Click Element                      ${NAV_EQUIPOS}