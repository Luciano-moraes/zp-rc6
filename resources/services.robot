**Settings***

Documentation   Camada de serviçoes de projeto de automação

Library       Collections                                                                                 
Library       RequestsLibrary        

Resource      helpers.robot

***Variables***

${base_uri}       http://zepalheta-api:3333

***Keywords***
Get Session Token
    ${resp}=    Post session    admin@zepalheta.com.br      pwd123

    ${token}    Convert To String     Bearer ${resp.json()['token']}
    [Return]     ${token}


POST session
    [Arguments]     ${email}     ${password}  

    ${result}=        Create Session    ZP           ${base_uri}
    &{headers}=       Create Dictionary         Content-Type=application/json
    &{payload}=       Create Dictionary         email=${email}       password=${password}
    

    ${resp}=      POST on session      ZP     /sessions       json=${payload}      headers=${headers}     expected_status=any

    [Return]                ${resp}

#POST customers
Post Customer
    [Arguments]       ${payload} 


    Create Session    ZP           ${base_uri}

    ${token}=   Get Session Token
    &{headers}=       Create Dictionary         Content-Type=application/json   Authorization=${token}
    ${resp}=      POST on session      ZP     /customers      json=${payload}      headers=${headers}     expected_status=any
    
    [Return]                ${resp}

# PUT /customers
Put Customer
    [Arguments]     ${payload}      ${user_id}

    Create Session      ZP      ${base_uri}

    ${token}=       Get Session Token
    &{headers}=     Create Dictionary       content-type=application/json   authorization=${token}

    ${resp}=        PUT On Session        ZP      /customers/${user_id}      json=${payload}     headers=${headers}   expected_status=any

    [return]        ${resp}


Get Customers
    
    Create Session    ZP           ${base_uri}

    ${token}=         Get Session Token
    &{headers}=       Create Dictionary         Content-Type=application/json      Authorization=${token}

    ${resp}=       Get on session      ZP     /customers      headers=${headers}    expected_status=any
    
    [Return]                ${resp}

Get Unique Customer
    [Arguments]     ${user_id}

    Create Session    ZP           ${base_uri}

    ${token}=       Get Session Token
    &{headers}=     Create Dictionary         Content-Type=Application/json     Authorization=${token}

    ${resp}=         Get on session      ZP       /customers/${user_id}      headers=${headers}   expected_status=any

    [return]        ${resp}

# DELETE /customers
Delete Customer
    [Arguments]        ${cpf}

    ${token}=        Get Session Token
    &{headers}=       Create Dictionary         Content-Type=application/json    Authorization=${token}

    ${resp}=         DELETE On Session    ZP      /customers/${cpf}     headers=${headers}    expected_status=any
    
    [return]        ${resp}



# POST /equipos
Post Equipo
    [Arguments]     ${payload}

    Create Session     ZP       ${base_uri}

    ${token}=       Get Session Token
    &{headers}=     Create Dictionary       content-type=application/json   authorization=${token}

    ${resp}=         POST on session       ZP       /equipos      data=${payload}     headers=${headers}   expected_status=any

    [return]        ${resp}

