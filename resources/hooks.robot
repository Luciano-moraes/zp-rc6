***Settings***
Library     SeleniumLibrary

***Keywords***
Start Session
   Open Browser  about:blank         gc
   Maximize Browser Window

Finish Session
    Close Browser

Login Session 
    Start Session
    Go TO          ${base_url} 
    Login With     ${admin_user}     ${admin_pass} 