***Settings***
Documentation     Representação a pagina clientes com seus elementos e ações


***Variables***
${CUSTOMERS_FORM}      css:a[href$=register]  
${LAB_NAME}            css:label[for=name]                         
${LAB_CPF}             css:label[for=cpf]
${LAB_ADDRESS}         css:label[for=address] 
${LAB_PHONE}           css:label[for=phone_number]
${CUSTOMER_LIST}       css:table

***Keywords***
Customer New client
    [Arguments]     ${name}    ${CPF}      ${address}      ${fone}
    Input Text      id:name               ${name}
    Input Text      id:cpf                ${cpf}
    Input Text      id:address            ${address}
    Input Text      id:phone_number       ${fone}
    Wait Until Element Is Not Visible     ${TOASTER_SUCCESS}      10
    Click Element   xpath://button[text()='CADASTRAR']

Go To Customer Details
    [Arguments]           ${cpf_formatado}
    
    ${element}=           Set Variable       xpath://td[text()='${cpf_formatado}'] 

    Wait Until Element Is Visible     ${element}    5
    Click Element                     ${element}

Click Remove Customer
    ${element}=           Set Variable       xpath://button[text()='APAGAR']
    Wait Until Element Is Visible            ${element}    5
    Click Element                            ${element}
         