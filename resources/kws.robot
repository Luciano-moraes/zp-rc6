***Settings***

Library          SeleniumLibrary


***Keywords***
#login
Acesso a area logado
   Go TO      ${base_url} 
  
Submeto minhas credenciais
    [Arguments]     ${email}    ${senha}

    Login With        ${email}    ${senha}

Devo ver a area logada
    Wait Until Page Contains   Aluguéis     5
    

Devo ver um toaster com a mensagem
    [Arguments]     ${message}

    Wait Until Element Contains    ${TOASTER_ERROR_P}     ${message}      
  
#Customers

Dado que acesso o formulário de cadastro de clientes
   Go To Customers
   Wait Until Element Is Visible      ${CUSTOMERS_FORM}         5
   Click Element                      ${CUSTOMERS_FORM}

E que eu tenha o seguinte cliente:
    [Arguments]          ${name}     ${cpf}      ${address}      ${fone}

    Remove Customer By Cpf        ${cpf}

    Set Test Variable     ${name}
    Set Test Variable      ${cpf}
    Set Test Variable      ${address}
    Set Test Variable      ${fone}

Mas esse cpf ja existe no sistema
    Insert Customer      ${name}     ${cpf}      ${address}      ${fone}

Quando faço a inclusão desse cliente
    Customer New client   ${name}     ${cpf}      ${address}      ${fone}

Então devo ver notificação de erro:  
    [Arguments]          ${expect_error} 
    Wait Until Element Contains       ${TOASTER_ERROR}         ${expect_error}    5  

Então devo ver notificação: 
    [Arguments]            ${expect_notice} 
    Wait Until Element Contains       ${TOASTER_SUCCESS}         ${expect_notice}    5

Então devo ver mensagem iformando que campos do clientes são Obrigatorios
    Wait Until Element Contains       ${LAB_NAME}              Nome é obrigatório        5
    Wait Until Element Contains       ${LAB_CPF}               CPF é obrigatório         5
    Wait Until Element Contains       ${LAB_ADDRESS}           Endereço é obrigatório    5
    Wait Until Element Contains       ${LAB_PHONE}             Telefone é obrigatório    5

Então devo ver texto:      
    [Arguments]          ${message} 
    Wait Until Page Contains       ${message}     5

E esse cliente deve aparecer na lista
   ${cpf_formatado}=      Format Cpf        ${cpf}
   Go Back
   Wait Until Element Is Visible      ${CUSTOMER_LIST}    5 
   Table Should Contain               ${CUSTOMER_LIST}        ${cpf_formatado}


##Remove Customer

Dado que eu tenho um cliente indesejado:
    [Arguments]         ${name}     ${cpf}     ${address}    ${phone_number}
    
    Remove Customer By Cpf          ${cpf}
    Insert Customer     ${name}     ${cpf}     ${address}    ${phone_number}

    Set Test Variable   ${cpf}

E acesso a lista de clientes
    Go To Customers

Quando eu removo esse cliente
    ${cpf_formatado}=         Format Cpf      ${cpf}

    Set Test Variable                 ${cpf_formatado}
    Go To Customer Details            ${cpf_formatado}
    Click Remove Customer

E esse cleiente não deve aparecer ma lista
    Wait Until Page Does Not Contain     ${cpf_formatado}

#Equipos
Dado que acesso o formulário de cadastro de Equipamentos
   Go To Equipos
   Wait Until Element Is Visible      ${EQUIPOS_FORM}         5
   Click Element                      ${EQUIPOS_FORM}

Quando faço a inclusão de um Equipamento
  Customer New Equipo    ${equipamento}        ${vl_diaria}

E esse Equipemanto deve aparecer na lista 
  Wait Until Page Does Not Contain     ${equipamento}

E que eu tenha o seguinte Equipamento:
    [Arguments]          ${equipamento}        ${vl_diaria}

    Remove Equipo        ${equipamento} 

    Set Test Variable    ${equipamento}
    Set Test Variable    ${vl_diaria}

Mas esse Equipamento ja existe no sistema
    Insert Equipo      ${equipamento}        ${vl_diaria}

