***Settings***
Documentation       Cadastro de clientes
Resource      ../../resources/base.robot
Suite Setup        Login Session
Suite Teardown     Finish Session

***Test Cases***
Novo cliente
    [tags]     smoke
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenha o seguinte cliente:
     ...     Jack Jonson         10000001406       Rua dos Bugs 2000    11999999998
    Quando faço a inclusão desse cliente
    Então devo ver notificação:     Cliente cadastrado com sucesso!
    E esse cliente deve aparecer na lista

Cliente duplicado
    [tags]     dup
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenha o seguinte cliente:
     ...     Linkin Park    13000001406       Rua dos Bugs 2000    11999999995
    Mas esse cpf ja existe no sistema
    Quando faço a inclusão desse cliente
    Então devo ver notificação de erro:     Este CPF já existe no sistema!

Campos obrigatório
    [tags]     obg
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenha o seguinte cliente:
     ...     ${EMPTY}     ${EMPTY}     ${EMPTY}     ${EMPTY}
    Quando faço a inclusão desse cliente
    Então devo ver mensagem iformando que campos do clientes são Obrigatorios

Nome Obrigatório  
    [Tags]          required
    [Template]          Tentativa de Cadastro de Clientes
    ${EMPTY}             00000002910       Rua dos Testes, 33       1199999999        Nome é obrigatório

CPF Obrigatório   
    [Tags]          required  
    [Template]          Tentativa de Cadastro de Clientes    
    Jack Jonson          ${EMPTY}             Rua dos Testes, 33          1199999999       CPF é obrigatório

Endereço Obrigatório 
    [Tags]          required
    [Template]           Tentativa de Cadastro de Clientes
    Jack Jonson           00000002910         ${EMPTY}                  1199999999       Endereço é obrigatório

Telefone Obrigatório 
    [Tags]          required  
    [Template]           Tentativa de Cadastro de Clientes 
    Jack Jonson              00000002910         Rua dos Testes, 33      ${EMPTY}        Telefone é obrigatório

Telefone Invalido  
    [Tags]          required 
    [Template]          Tentativa de Cadastro de Clientes 
    Jack Jonson          00000002910      Rua dos Testes, 33      119999999       Telefone inválido


***Keywords***
Tentativa de Cadastro de Clientes
    [Arguments]     ${nome}     ${cpf}      ${endereco}      ${telefone}     ${saida}
    Dado que acesso o formulário de Cadastro de Clientes
    E que eu tenha o seguinte cliente:
    ...     ${nome}        ${cpf}      ${endereco}     ${telefone} 
    Quando faço a inclusão desse Cliente
    Então devo ver texto:      ${saida}




  
