***Settings***
Documentation       Cadastro de Equipamentos
Resource           ../../resources/base.robot
Suite Setup        Login Session
Suite Teardown     Finish Session

***Test Cases***
Novo Equipamentos
  Dado que acesso o formulário de cadastro de Equipamentos
  E que eu tenha o seguinte Equipamento:
  ...     Violão        100
  Quando faço a inclusão de um Equipamento
  Então devo ver notificação:      Equipo cadastrado com sucesso!
  E esse Equipemanto deve aparecer na lista 


Nome Obrigatório  
    [Tags]          Cobrigatorio
    [Template]          Tentativa de Cadastro de Equipamento
    ${EMPTY}             100           Nome do equipo é obrigatório  

Valor Obrigatório   
    [Tags]           Cobrigatorio 
    [Template]          Tentativa de Cadastro de Equipamento   
    Guitarra       ${EMPTY}            Diária do equipo é obrigatória


Equipamento Duplicado
    [Tags]     dup
    Dado que acesso o formulário de cadastro de Equipamentos
    E que eu tenha o seguinte Equipamento:
    ...     violino        100
    Mas esse Equipamento ja existe no sistema
    Quando faço a inclusão de um Equipamento
    Então devo ver notificação de erro:     Erro na criação de um equipo

***Keywords***

Tentativa de Cadastro de Equipamento  
    [Arguments]     ${equipamento}        ${vl_diaria}      ${saida}
    Dado que acesso o formulário de cadastro de Equipamentos
    E que eu tenha o seguinte Equipamento:
    ...     ${equipamento}        ${vl_diaria}
    Quando faço a inclusão de um Equipamento
    Então devo ver texto:      ${saida}



  