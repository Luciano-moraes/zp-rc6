***Settings***
Documentation       Exclusão de clientes
Resource           ../../resources/base.robot
Suite Setup        Login Session
Suite Teardown     Finish Session

***Test Cases***
Remover Cliente
    Dado que eu tenho um cliente indesejado:
    ...      Michael Jackson      12345678901     Rua dos Bugs,333        48988375050
    E acesso a lista de clientes
    Quando eu removo esse cliente
    Então devo ver notificação:   Cliente removido com sucesso!
    E esse cleiente não deve aparecer ma lista
  


   