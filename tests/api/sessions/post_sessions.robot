***Settings***

Resource   ../../../resources/services.robot


***Test Cases***

Login com sucesso
    ${resp}=      POST session     admin@zepalheta.com.br      pwd123     
    Status Should Be       200      ${resp}

Senha invalida
    ${resp}=      POST session       admin@zepalheta.com.br     abc123         
    Status Should Be        401            ${resp}

Usuario Invalido
    ${resp}=       POST session        401@zepalheta.com.br    pwd123           
    Status Should Be        401      ${resp}